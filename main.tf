terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.44.0"
    }
  }
}

provider "openstack" {
  auth_url = var.auth_url
  region = var.region
  user_name = var.user_name
  password = var.password
  user_domain_name = var.user_domain_name
  project_domain_id = var.project_domain_id
  tenant_id = var.tenant_id
  tenant_name = var.tenant_name
}

resource "openstack_compute_keypair_v2" "cluster_k8s_lab_keypair" {
  name = "cluster_k8s_lab_keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0O4znBcbEcph6RTA0MJr++FMO9QwGeIPwiRhVnrhuEMoQ8AkLb2jpoeHp8C5pkBXesazsbl//wukSd89IXechBEM4mUO2ydKZajpjvhXQoR/XQKMn5L9eVwzGNAYpGbp9GJM6eMIWdJltZZfVmYTSbP6mWSXYirF+TadVFt/3B8wHU4YSWeFl+WhWfuS4Vqk/7wRmOhj2vPfhHsTzfh4hS0qWhDu7ELu0TlqWjc4WwyxBrib0lcQgmlrGPbVPs9/psU2gBCaRtmqjBmbSvrxa795d5GUlUT5pjRfNbPBQ/ABKKLnTFGagx5Uzwjs0P0Bm9TZA4oZ6oGHv44vwmQAT+wcVMoJYkDgOhPaKq+6nN2VAU5rHiMtE+80G+SrPc5wUz8b3+/DmJwKxpNvfo4lY9ktma370TeUVwB/A4tVvuQKjWJltJ085Iy9lE4yr8UUeFtfSEN56dWU3xmWN//1mdk4jqP5pCBeRjYP26h0POm1WR2c+CAZjcHDHQ6/z9ci4A+B+yUPJ8TIjcyCugTlsw+hXDSNfKN77WqYbaxhVc8/HZCjNNYqaKor3KVYmKyTNgcmhN+r8sNA4hnWWYy5GS4hQnVnzBqcWaFmn8y7v9ZPrLa0dl4x25x3AwSNFCvvHUQ3plqZUtYosBE7O0JHE2pFIvj91oBVfrFLRohKzwQ== zhu@zhu-VirtualBox"
}

resource "openstack_compute_instance_v2" "node_master" {
  name            = "node_master"
  image_id        = var.image_id
  flavor_name     = var.flavor_name
  key_pair        = "cluster_k8s_lab_keypair"
  security_groups = ["sg_cluster_k8s_lab"]

  metadata = {
    type = "node master k8s"
  }

   network {
    port = "${openstack_networking_port_v2.port_master_node.id}"
  }

  floating_ip = "${openstack_networking_floatingip_v2.floatip_port_master_node.address}"
}

resource "openstack_compute_instance_v2" "node_worker_mars" {
  name            = "node_worker_mars"
  image_id        = var.image_id
  flavor_name     = var.flavor_name
  key_pair        = "cluster_k8s_lab_keypair"
  security_groups = ["sg_cluster_k8s_lab"]

  metadata = {
    type = "node worker mars k8s"
  }

  network {
    port = "${openstack_networking_port_v2.port_worker_node_mars.id}"
  }

  floating_ip = "${openstack_networking_floatingip_v2.floatip_port_worker_node_mars.address}"
}

resource "openstack_compute_instance_v2" "node_worker_venus" {
  name            = "node_worker_venus"
  image_id        = var.image_id
  flavor_name     = var.flavor_name
  key_pair        = "cluster_k8s_lab_keypair"
  security_groups = ["sg_cluster_k8s_lab"]

  metadata = {
    type = "node worker venus k8s"
  }

  network {
    port = "${openstack_networking_port_v2.port_worker_node_venus.id}"
  }

  floating_ip = "${openstack_networking_floatingip_v2.floatip_port_worker_node_venus.address}"
}
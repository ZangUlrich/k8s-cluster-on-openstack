resource "null_resource" "ansible-provision" {

  depends_on = [openstack_compute_instance_v2.node_master, openstack_compute_instance_v2.node_worker_mars, openstack_compute_instance_v2.node_worker_venus]


  provisioner "local-exec" {
    command = "echo \"[all:vars]\" > inventory/inventory"
  }

  provisioner "local-exec" {
    command = "echo \"ansible_ssh_private_key_file=ssh_keys/cluster_k8s_lab_keypair\" >> inventory/inventory"
  }

  ##Create Masters Inventory
  provisioner "local-exec" {
    command = "echo \"\n[kube-master]\n${openstack_compute_instance_v2.node_master.name} ansible_ssh_host=${openstack_networking_floatingip_v2.floatip_port_master_node.address}\" >> inventory/inventory"
  }

  ##Create Nodes Inventory
  provisioner "local-exec" {
    command = "echo \"\n[kube-node]\" >> inventory/inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${openstack_compute_instance_v2.node_worker_mars.name} ansible_ssh_host=${openstack_networking_floatingip_v2.floatip_port_worker_node_mars.address}\" >> inventory/inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${openstack_compute_instance_v2.node_worker_venus.name} ansible_ssh_host=${openstack_networking_floatingip_v2.floatip_port_worker_node_venus.address}\" >> inventory/inventory"
  }

  #provisioner "local-exec" {
  #  command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory/inventory -u ubuntu -b ansible/playbooks/k8s_cluster_setup.yml -vv"
  #}
}

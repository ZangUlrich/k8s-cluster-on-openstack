#!/bin/bash

# Enable kubelet
sudo systemctl enable kubelet

# Pull container images for etcd, api server, etc ...
sudo kubeadm config images pull --cri-socket=/run/containerd/containerd.sock

# Initialize the control plane
sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --cri-socket=/run/containerd/containerd.sock

# Set up kubeconfig for the current user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Install calico network plugin for pods communication
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/tigera-operator.yaml
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/custom-resources.yaml

# Generate the join command for worker nodes
join_command=$(kubeadm token create --print-join-command)

# Join worker nodes to the cluster
echo "Run the following command on worker nodes to join them to the cluster:"
echo "$join_command"

# cluster status
kubectl cluster-info

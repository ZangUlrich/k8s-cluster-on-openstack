variable "auth_url" {
  type    = string
  default = "https://api.pub1.infomaniak.cloud/identity"
}

variable "region" {
  type    = string
  default = "dc3-a"
}

variable "user_name" {
  type    = string
}

variable "user_domain_name" {
  type    = string
  default = "Default"
}

variable "password" {
  type = string
  sensitive = true
}

variable "project_domain_id" {
  type    = string
  default = "default"
}

variable "tenant_id" {
  type    = string
  sensitive = true
}

variable "tenant_name" {
  type    = string
}

variable "image_id" {
    type = string
    default = "ff2e2c2f-20d1-49f8-990b-c18e5ccdded5"
}

variable "flavor_name" {
  type = string
  default = "a4-ram16-disk80-perf1"
  description = "a4-ram16-disk80-perf1 has 4 CPUs, 16 GB of RAM and 80 GB of Storage"
}

variable "ext-floating1-ID" {
  type = string
  default = "0f9c3806-bd21-490f-918d-4a6d1c648489"
  description = "ext-floating1 ID"
}
resource "openstack_networking_network_v2" "network_cluster_k8s_lab" {
  name           = "network_cluster_k8s_lab"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_cluster_k8s_lab" {
  name       = "subnet_cluster_k8s_lab"
  network_id = "${openstack_networking_network_v2.network_cluster_k8s_lab.id}"
  cidr       = "10.0.0.0/24"
  gateway_ip = "10.0.0.1"
  dns_nameservers = ["1.1.1.1"]
  ip_version = 4
}


resource "openstack_networking_router_v2" "external_router" {
  name                = "external_router"
  admin_state_up      = true
  external_network_id = var.ext-floating1-ID
}

resource "openstack_networking_router_interface_v2" "int_1" {
  router_id = "${openstack_networking_router_v2.external_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_cluster_k8s_lab.id}"
}


resource "openstack_compute_secgroup_v2" "sg_cluster_k8s_lab" {
  name        = "sg_cluster_k8s_lab"
  description = "Security Group for the cluster K8s Lab"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 443
    to_port     = 443
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "tcp"
    cidr        = "10.0.0.0/24"
  }

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "udp"
    cidr        = "10.0.0.0/24"
  }

  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "10.0.0.0/24"
  }
}

resource "openstack_networking_port_v2" "port_master_node" {
  name               = "port_master_node"
  network_id         = "${openstack_networking_network_v2.network_cluster_k8s_lab.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.sg_cluster_k8s_lab.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_cluster_k8s_lab.id}"
    ip_address = "10.0.0.10"
  }
}

resource "openstack_networking_port_v2" "port_worker_node_venus" {
  name               = "port_worker_node_venus"
  network_id         = "${openstack_networking_network_v2.network_cluster_k8s_lab.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.sg_cluster_k8s_lab.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_cluster_k8s_lab.id}"
    ip_address = "10.0.0.20"
  }
}

resource "openstack_networking_port_v2" "port_worker_node_mars" {
  name               = "port_worker_node_mars"
  network_id         = "${openstack_networking_network_v2.network_cluster_k8s_lab.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.sg_cluster_k8s_lab.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_cluster_k8s_lab.id}"
    ip_address = "10.0.0.30"
  }
}


resource "openstack_networking_floatingip_v2" "floatip_port_master_node" {
    pool = "ext-floating1"
    port_id = "${openstack_networking_port_v2.port_master_node.id}" 
}

resource "openstack_networking_floatingip_v2" "floatip_port_worker_node_venus" {
    pool = "ext-floating1"
    port_id = "${openstack_networking_port_v2.port_worker_node_venus.id}" 
}

resource "openstack_networking_floatingip_v2" "floatip_port_worker_node_mars" {
    pool = "ext-floating1"
    port_id = "${openstack_networking_port_v2.port_worker_node_mars.id}" 
}
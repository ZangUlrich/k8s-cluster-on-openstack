# Kubernetes cluster on openstack

This repository purpose is to setup a **kubernetes cluster** with **Terraform** and **Ansible**, using **openstack provider**.

* **kubeadm** is use to bootstrap the k8s cluster 
* **containerd** is use as container runtime
* **calico** is use as network plugin for pods

**N.B:** It is important to note that the cloud provider [infomaniak](https://www.infomaniak.com/) which use openstack in background has been use for proof of concept.

The architecture is made up of **one master node** and **two worker nodes**.

![Architecture](infra_lab.png)


## Requirements

* **Terraform latest version**
* **Ansible** 
* You need to generate a ssh key with the name **cluster_k8s_lab_keypair** in the subdirectory in this project **ssh_keys/**, you can use `ssh-keygen` for that.
* For the **required variables** in **variables.tf**, you can define some variable envionment as [Terraform](https://developer.hashicorp.com/terraform/cli/config/environment-variables) describe it with the *TF_VAR_* prefix. 


## Quick Start

```bash
terraform plan
terraform apply -auto-approve

# Wait for nodes to be reachable
sleep 10

# Run the playbook to setup the k8s cluster 
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory/inventory -u ubuntu -b ansible/playbooks/k8s_cluster_setup.yml -vv
```

**N.B:**  Don't worry for the inventory it is automatically generated when building the architecture, in the **create_ansible_inventory.tf** terraform file.


After the ansible playbook is finished **copy** the output `kubeadm join 10.0.0.10:6443 --token <TOKEN> --discovery-token-ca-cert-hash sha256:<HASH>` locate at the end of the output, just after the notice **Run the following command on worker nodes to join them to the cluster**, and exectute that command in the two worker nodes (**venus** and **mars**) as sudo to make them **join** the k8s cluster.